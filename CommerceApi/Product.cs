public class Product
{
    public int Id { get; set; }

    public required string Sku { get; set; }

    public required string Name { get; set; }

    public decimal Price { get; set; }

    public required string Currency { get; set; }

    public int WeightInGr { get; set; }

    public required Category Category { get; set; }
}