using Microsoft.EntityFrameworkCore;

class CommerceDb : DbContext
{
    public CommerceDb(DbContextOptions<CommerceDb> options) : base(options)
    {
    }

    public DbSet<Category> Categories => Set<Category>();

    public DbSet<Product> Products => Set<Product>();
}