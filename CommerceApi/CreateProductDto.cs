public class CreateProductDto
{
    public required string Sku { get; set; }

    public required string Name { get; set; }   

    public decimal Price { get; set; }

    public string? Currency { get; set; }

    public required string CategoryId { get; set; }

    public int WeightInGr { get; set; }        
}