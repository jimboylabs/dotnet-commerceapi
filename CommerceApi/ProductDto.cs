public class ProductDto
{
    public required string Id { get; set; }  

    public required string Sku { get; set; }

    public required string Name { get; set; }   

    public decimal Price { get; set; }

    public string? Currency { get; set; }

    public required CategoryDto Category { get; set; }

    public int WeightInGr { get; set; }        
}