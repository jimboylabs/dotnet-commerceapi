public class CategoryDto
{
    public required string Id { get; set; }
    public string? Name { get; set; }
}