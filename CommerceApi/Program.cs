using HashidsNet;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");

builder.Services.AddSingleton<IHashids>(_ => new Hashids("CommerceApi", 11));
builder.Services.AddDbContext<CommerceDb>(options => options.UseNpgsql(connectionString));

builder.Services.AddDatabaseDeveloperPageExceptionFilter();

var app = builder.Build();

app.MapGet("/", () => "Hello World!");

var categories = app.MapGroup("categories");
var products = app.MapGroup("products");

categories.MapGet("/", GetAllCategories);
categories.MapGet("/{id}", GetCategory);
categories.MapPost("/", CreateCategory);

products.MapGet("/", GetAllProducts);
products.MapPost("/", CreateProduct);

static async Task<IResult> GetAllCategories(CommerceDb db, IHashids _hashids)
{
    return TypedResults.Ok(await db.Categories.Select(c => new CategoryDto
    {
        Id = _hashids.Encode(c.Id),
        Name = c.Name
    }).ToArrayAsync());
}

static async Task<IResult> GetCategory(string id, CommerceDb db, IHashids _hashids)
{
    var rawId = _hashids.Decode(id);
    if (rawId.Length == 0)
    {
        return TypedResults.NotFound();
    }

    return await db.Categories.FindAsync(rawId[0])
        is Category category
        ? TypedResults.Ok(new CategoryDto
        {
            Id = id,
            Name = category.Name
        }) : TypedResults.NotFound();
}

static async Task<IResult> CreateCategory(Category category, CommerceDb db, IHashids _hashids)
{
    db.Categories.Add(category);
    await db.SaveChangesAsync();

    var hashId = _hashids.Encode(category.Id);
    return TypedResults.Created($"/categories/{hashId}", new CategoryDto
    {
        Id = hashId,
        Name = category.Name
    });
}

static async Task<IResult> GetAllProducts([FromQuery] int? page, [FromQuery(Name = "page_size")] int? pageSize, CommerceDb db, IHashids _hashids)
{
    var pageParam = PageParam.Parse(page ?? 0, pageSize ?? 0, 20);

    return TypedResults.Ok(await db.Products.
        OrderBy(p => p.Id).
        Skip(pageParam.Offset).
        Take(pageParam.Limit).
        Select(p => new ProductDto
        {
            Id = _hashids.Encode(p.Id),
            Name = p.Name,
            Sku = p.Sku,
            WeightInGr = p.WeightInGr,
            Price = p.Price,
            Currency = p.Currency,
            Category = new CategoryDto
            {
                Id = _hashids.Encode(p.Category.Id),
                Name = p.Category.Name
            }
        }).ToArrayAsync());
}

static async Task<IResult> CreateProduct(CreateProductDto createProduct, CommerceDb db, IHashids _hashids)
{
    var categoryRawId = _hashids.Decode(createProduct.CategoryId);

    if (categoryRawId.Length == 0)
    {
        return TypedResults.NotFound();
    }

    var category = await db.Categories.FindAsync(categoryRawId[0]);
    if (category is null)
    {
        return TypedResults.NotFound();
    }

    var product = new Product
    {
        Name = createProduct.Name,
        Sku = createProduct.Sku,
        Price = createProduct.Price,
        WeightInGr = createProduct.WeightInGr,
        Currency = "IDR",
        Category = category
    };

    db.Products.Add(product);
    await db.SaveChangesAsync();

    var hashId = _hashids.Encode(product.Id);
    return TypedResults.Created($"/products/{hashId}", new ProductDto
    {
        Id = hashId,
        Name = product.Name,
        Sku = product.Sku,
        Price = product.Price,
        WeightInGr = product.WeightInGr,
        Currency = product.Currency,
        Category = new CategoryDto
        {
            Id = createProduct.CategoryId,
            Name = category.Name
        }
    });
}

app.Run();
