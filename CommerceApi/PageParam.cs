public class PageParam
{
    public int Page { get; set; }
    public int PageSize { get; set; }
    public int Offset { get; set; }
    public int Limit { get; set; }

    public PageParam(int page, int pageSize, int offset, int limit)
    {
        Page = page;
        PageSize = pageSize;
        Offset = offset;
        Limit = limit;
    }

    public static PageParam Parse(int page, int pageSize, int defaultPageSize)
    {
        var newPage = page;
        if (newPage <= 0)
        {
            newPage = 1;
        }

        var newPageSize = pageSize;
        if (newPageSize <= 0)
        {
            newPageSize = defaultPageSize;
        }

        return new PageParam(newPage, newPageSize, (newPage - 1) * newPageSize, newPageSize + 1);
    }
}
