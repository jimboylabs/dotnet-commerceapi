namespace CommerceApi.Tests;

public class PageParamTest
{
    [Fact]
    public void Parse_WithValidInputs_ReturnsPageParam()
    {
        var page = 1;
        var pageSize = 10;
        var defaultPageSize = 20;

        var result = PageParam.Parse(page, pageSize, defaultPageSize);

        Assert.Equal(1, result.Page);
        Assert.Equal(10, result.PageSize);
        Assert.Equal(0, result.Offset);
        Assert.Equal(11, result.Limit);
    }

    [Fact]
    public void Parse_WithZeroPageSize_UsesDefaultPageSize()
    {
        var page = 1;
        var pageSize = 0;
        var defaultPageSize = 20;

        var result = PageParam.Parse(page, pageSize, defaultPageSize);

        Assert.Equal(defaultPageSize, result.PageSize);
    }
}